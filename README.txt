.. contents::

Installation
============

A Makefile is used for installing:

    make build

Run the Pyramid server
=======================

Using the grape_example.csv file:

    ./bin/pserve development_grape_python.ini 

Using the hg19_example.csv file:

    ./bin/pserve development_hg19_python.ini 

Using a Solr backend:

    ./bin/pserve development_solr.ini 

Using a Solr backend:

    ./bin/pserve production.ini 

Browsing
========

When the server is running, point your browser to

    http://0.0.0.0:6543

The CSV file 

    grape_example.csv

is used.
