import codecs


def parser(file_path):
    all_lines = codecs.open(file_path, 'r', encoding='utf-8').read().split('\n')
    header = all_lines[0].split(u'\t')
    count = 0
    lines = []
    for line in all_lines[1:-1]:
        count += 1
        lines.append(dict(zip(header, line.split('\t'))))
        lines[-1]['typestatus'] = 'empty'
        lines[-1]['geo_locality'] = lines[-1]['latitude'] + ',' + lines[-1]['longitude']
        if count > 100:
            break
    return lines
