import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

entry_points = """\
      [paste.app_factory]
      main = grape.pyramid:main
      """

classifiers = [
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ]

requires = [
     'pyramid',
     'pyramid_chameleon',
     'pyramid_debugtoolbar',
     'pyramid_tm',
     'pyramid_zodbconn',
     'transaction',
     'ZODB3',
     'waitress',
     'requests',
     'simplejson',
     'lxml',
     'httplib2',
     'simplejson',
     'zope.schema',
     'plone.batching',
     'AccessControl',
     'python-dateutil'
    ]

setup(name='grape.pyramid',
      version='1.0',
      packages=find_packages(),
      description='grape.pyramid',
      long_description=README + '\n\n' + CHANGES,
      author='',
      author_email='',
      include_package_data=True,
      zip_safe=False,
      classifiers=classifiers,
      install_requires=requires,
      keywords='web pylons pyramid',
      url='',
      license='gpl',
      namespace_packages=['grape'],
      entry_points=entry_points,
      tests_require=requires,
      test_suite="grape.pyramid",
      )
