import os
import csv
import simplejson
from dateutil.parser import parse
import codecs


def grape_parser(file_path):
    all_lines = codecs.open(file_path, 'r', encoding='utf-8').read().split('\n')
    header = all_lines[0].split(u',')
    count = 0
    lines = []
    for line in all_lines[1:-1]:
        count += 1
        lines.append(dict(zip(header, line.split(','))))
        lines[-1]['asset_type'] = 'whatever'
    return lines


def unfold(lines):
    """
    Unfold the lines
    """
    unfolded = []
    line_number = 0
    for line in lines:
        line_number = line_number + 1
        file_name, attrs = line.split('\t')
        new_line = {}
        new_line["id"] = line_number
        new_line["file_name"] = file_name
        for attr in attrs.split(';'):
            attr = attr.strip()
            if attr:
                key, value = attr.split('=')
                key = key.strip()
                value = value.strip()
                new_line[key] = value
        unfolded.append(new_line)
    return unfolded

def hg19_parser(file_path):
    fieldnames = ["id", "labExpId", "quality", "readType", "paired", "sex", "url", "file_name", "view", "type",
                  "dataType", "organism", "project_name", "organisation", "group", "user", "date", "md5",
                  'asset_type',

                  'size',
                  'md5sum',
                  'dataVersion',
                  'subId',
                  'dccAccession',
                  'dateSubmitted',
                  'tableName',
                  'grant',
                  'cell',
                  'composite',
                  'dateUnrestricted',
                  'localization',
                  'lab',
                  'rnaExtract',
                  'origAssembly',
                  'project',
                  'submittedDataVersion',
                  'replicate',
                  'insertLength',
                  'mapAlgorithm',
                  'dateResubmitted',
                  'geoSampleAccession',
                  'labVersion',
                  'treatment',
                  'seqPlatform',
                  'bioRep',
                  'spikeInPool',
                  'labProtocolId',
                  'donorId',
                  'protocol',
                  'softwareVersion',
                  'fileLab',
                  'dateProcessed',
                  ]

    index = unfold(open(file_path, 'r').readlines())

    row = {'id': '',
           'organism': '',
           'project_name': '',
           'organisation': '',
           'group': '',
           'user': '',
           'asset_type': "",
           "labExpId": "",
           "quality": "",
           "readType": "",
           "paired": "",
           "sex": "",
           "url": "",
           "file_name": "",
           "view": "",
           "type": "",
           "dataType": "",
           "organism": "",
           "project_name": "",
           "organisation": "",
           "group": "",
           "user": "",
           "date": "",
           "md5": "",
           "asset_type": "",
           'size': "",
           'md5sum': "",
           'dataVersion': "",
           'subId': "",
           'dccAccession': "",
           'dateSubmitted': "",
           'tableName': "",
           'grant': "",
           'cell': "",
           'composite': "",
           'dateUnrestricted': "",
           'localization': "",
           'lab': "",
           'rnaExtract': "",
           'origAssembly': "",
           'project': "",
           'submittedDataVersion': "",
           'replicate': "",
           'insertLength': "",
           'mapAlgorithm': "",
           'dateResubmitted': "",
           'geoSampleAccession': "",
           'labVersion': "",
           'treatment': "",
           'seqPlatform': "",
           'bioRep': "",
           'spikeInPool': "",
           'labProtocolId': "",
           'donorId': "",
           'protocol': "",
           'softwareVersion': "",
           'fileLab': "",
           'dateProcessed': "",           
          }

    rows = []
    counter = 100
    for item in index:
        counter = counter + 1
        new_row = row.copy()
        new_row.update(item)
        new_row.update({'id': str(counter),
                        'organism': 'human',
                        'project_name': 'ENCODE',
                        'organisation': 'ENCODE',
                        'group': 'rguigo',
                        'user': 'rguigo',
                        'asset_type': "data"})
        rows.append(new_row)
    return rows
