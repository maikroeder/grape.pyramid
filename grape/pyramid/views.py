from pyramid.view import view_config

class GrapeViews(object):

    def __init__(self, context, request):
        self.context = context
        self.request = request

    @view_config(renderer='like.solr:templates/index.pt')
    def default_view(self):
        return self.context.index()
