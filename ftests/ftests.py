import requests

#
# GET Home page
#
r = requests.get('http://0.0.0.0:6543')
#import pdb; pdb.set_trace()
assert(r.headers['content-type'] == 'text/html; charset=UTF-8')
assert(r.status_code == 200)

url = "http://0.0.0.0:6543/search?q=Jervis&fq=fullScientificName:%22hertel%22"
r = requests.get(url)

import pdb; pdb.set_trace()
